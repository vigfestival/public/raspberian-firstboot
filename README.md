# raspberian-firstboot



## Getting started


```
cd /opt
git clone https://gitlab.com/vigfestival/public/raspberian-firstboot.git
cd /opt/raspberian-firstboot
```



# Mount img
1. sudo chmod +x [mount-raspbian-image](mount-raspbian-image)
1. sudo ./mount-raspbian-image  xxxx.img
1. sudo df -h

#  Add firstboot.service to the root 
1. [firstboot.service](firstboot.service) is installed in `sudo cp -r /opt/raspberian-firstboot/firstboot.service /mnt/image/root/lib/systemd/system/firstboot.service`
1. [firstboot.service](firstboot.service) is enabled: `cd /mnt/image/root/etc/systemd/system/multi-user.target.wants && sudo ln -s /lib/systemd/system/firstboot.service .`


# Umount
1. sudo unmount /mnt/image/*
1. sudo df -h

